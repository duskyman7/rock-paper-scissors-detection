//
// Created by duso on 06-Apr-17.
//

#ifndef OPENCVTEST_FRAMECONTAINER_H
#define OPENCVTEST_FRAMECONTAINER_H

#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class FrameContainer{
public:
    FrameContainer(unsigned int size = 10){
        maxsize = size;
        frames.resize(size);
        index_of_next_frame = 0;
        framecount = 0;
    }

    void setPreprocess(bool b){
        preprocess = b;
    }

    void addFrame(Mat &mat){
        if (preprocess){
            blur(mat,frames[index_of_next_frame],Size(3,3));
        }
        else{
            mat.copyTo(frames[index_of_next_frame]);
        }
        index_of_next_frame = (index_of_next_frame + 1) % maxsize;
        if (framecount < maxsize){
            framecount++;
        }
    }

    const vector<Mat>& getFrames(){
        return frames;
    }

    Mat getAverageImage()
    {
        Size size = frames[0].size();
        Mat frame = Mat::zeros(frames[0].size(),CV_32FC3);

        for (auto &f : frames){
            accumulate(f,frame);
        }

        frame /= frames.size();
        frame.convertTo(frame,CV_8UC3);
        return frame;
    }

    size_t size(){
        return frames.size();
    }

    void clear()
    {
        frames.clear();
        frames.resize(maxsize);
        framecount = 0;
    }

    bool ready(){
        return (framecount == maxsize);
    }

private:
    vector<Mat> frames;
    int index_of_next_frame;
    bool preprocess = true;
    unsigned int maxsize;
    int framecount;
};

#endif //OPENCVTEST_FRAMECONTAINER_H
