//
// Created by duso on 13-Apr-17.
//

#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

// Program na vytvaranie samplov pre trenovanie SVMiek. Zobrazi okno so zaznamom
// z kamery a zelenym ramcekom. Vase zvolene gesto ukazte v ramci ramceka. Stla-
// cenim medzernika sa ulozi snimka na pocitac (do working directory).

int main(int argc, char *argv[])
{
    string filename = "";

    if (argc > 1) {
        filename = argv[1];
    }

    VideoCapture vc(0);

    vc.set(CV_CAP_PROP_EXPOSURE, 0);
    vc.set(CV_CAP_PROP_GAIN, 0);

    int key;
    int i = 0;

    fstream fs;

    {
        do {
            fs.open(filename + to_string(i) + ".png");
            i++;
        } while (fs.good());
    }

    Mat frame;

    Size winSize(96,128);

    while ((key = waitKey(1)) != 0x1b) {
        vc.read(frame);
        Mat drawing(frame);
        Rect boundRect;
        boundRect.height = 300;
        boundRect.width = boundRect.height * 3 / 4;
        boundRect.x = frame.size().width / 2 - boundRect.width / 2;
        boundRect.y = frame.size().height / 2 - boundRect.height / 2;
        rectangle(drawing, boundRect.tl(), boundRect.br(), Scalar(0, 255, 0), 1);
        imshow("kamera", drawing);
        if (key == 32){
            Mat result;
            resize(frame(boundRect),result, winSize);
            Mat gray;
            cvtColor(result,gray,CV_BGR2GRAY);
            Mat equalized;
//            equalizeHist(gray,equalized);
            imwrite(filename + to_string(i) + ".png", gray);
            i++;
        }
    }
}