//
// Created by duso on 04-May-17.
//

#ifndef OPENCVTEST_FINDFILES_H
#define OPENCVTEST_FINDFILES_H

#include <vector>
#include <iostream>
#include <dirent.h>

using namespace std;

vector<string> findFilesInFolder(string path){
    DIR *dir = opendir(path.c_str());
    vector<string> entries;
    if (dir == nullptr){
        cerr << "ERROR: Could not open directory \"" << path << "\"" << endl;
        return entries;
    }

    dirent *iterator;

    while ((iterator = readdir(dir)) != nullptr){
        string fullpath = path + iterator->d_name;
        DIR *dir2 = opendir(fullpath.c_str());

        if (dir2 != nullptr){
            closedir(dir2);
            continue;
        }

        entries.emplace_back(iterator->d_name);
    }

    closedir(dir);

    return entries;
}

#endif //OPENCVTEST_FINDFILES_H
