//
// Created by duso on 05-May-17.
//

#include <iostream>
#include <map>
#include <opencv2/opencv.hpp>

#include "findFiles.h"

// inspired by example from http://docs.opencv.org/3.1.0/d1/d73/tutorial_introduction_to_svm.html

using namespace std;
using namespace cv;

#define MAX_EXAMPLE_NUM 1000

enum EnumFigures{
    KAMEN = 0,
    PAPIER,
    NOZNICE,
    POZADIE
};

map<int, string> labels{
        {KAMEN, "kamen"},
        {PAPIER, "papier"},
        {NOZNICE, "noznice"},
        {POZADIE, "pozadie"}
};

void computeDescriptors(string basepath, vector<string> &filenames, int index, vector<vector<float>> & descriptors, HOGDescriptor &hog){
    Mat resized;
    Mat image;
    Mat gray;
    if (index == POZADIE){
        for (int i=0; i<min((int)(filenames.size()),50);i++){
            string path = basepath+labels[index]+"/"+filenames[i];
            cout << path << endl;
            image = imread(path);
            cvtColor(image,gray,CV_BGR2GRAY);
            if (gray.cols <= 96 || gray.rows <= 128){
                cerr << "moc maly obrazok!!!" << endl;
                throw 9;
            }
            // take 20 random segments from image
            for (int j=0; j<20; j++){
                int x = rand() % (gray.cols-96);
                int y = rand() % (gray.rows-128);
                Mat segment = gray(Rect(x,y,96,128));
                vector<float> descriptors_tmp;
                hog.compute(segment, descriptors_tmp);
                descriptors.push_back(descriptors_tmp);
            }
        }
    }
    else{
        for (int i=0; i<min((int)(filenames.size()),MAX_EXAMPLE_NUM); i++){
            string path = basepath+labels[index]+"/"+filenames[i];
            cout << path << endl;
            image = imread(path);
            resize(image,resized,Size(96,128));
            vector<float> descriptors_tmp;
            hog.compute(resized, descriptors_tmp);
            descriptors.push_back(descriptors_tmp);
        }
    }
}

void printHelp(char *argv)
{
    cout << "Usage: " << argv << " [basepath]" << endl;
    cout << "    basepath - path to folder, which contains these subfolders: kamen, papier," << endl;
    cout << "               noznice and pozadie. Subfolders should contain corresponding ima-" << endl;
    cout << "               ges, which will be used in training the svm." << endl << endl;
}

void trainSVM(int index, vector<vector<vector<float>>> &descriptors)
{
    Ptr<ml::SVM> svm = ml::SVM::create();

    svm->setType(ml::SVM::C_SVC);
    svm->setC(1.0);
    svm->setKernel(ml::SVM::KernelTypes::LINEAR);
    svm->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6));

    vector<float> samples_merged;
    vector<int> responses;

    cout << "training svm for: " << labels[index] << endl;

    int descriptorSize = descriptors[0][0].size();
    int descriptorCount = 0;

    for (int i=0; i<descriptors.size(); i++){
        for (int j=0; j<descriptors[i].size(); j++){
            descriptorCount++;
            for(float f: descriptors[i][j]){
                samples_merged.push_back(f);
            }
            responses.push_back((i==index)?1:-1);
        }
    }

    Mat samplesMat(descriptorCount,descriptorSize,CV_32F,samples_merged.data());
    Mat responsesMat(descriptorCount,1,CV_32S,responses.data());

    svm->train(samplesMat,ml::ROW_SAMPLE,responsesMat);
    svm->save(string("svm_")+labels[index]);
}

int main(int argc, char *argv[]){
    HOGDescriptor hog(Size(96,128),Size(16,16), Size(8,8), Size(8,8), 9);

    if (argc > 1 && (string(argv[1]) == "-h" || string(argv[1]) == "--help") ){
        printHelp(argv[0]);
        return 0;
    }

    string basepath = "./";

    if (argc > 1){
        basepath = argv[1];
    }

    if (basepath[basepath.size()-1] != '/'){
        basepath += "/";
    }

    vector<vector<string>> filenames(4);

    filenames[KAMEN] = findFilesInFolder(basepath+labels[KAMEN]);
    filenames[PAPIER] = findFilesInFolder(basepath+labels[PAPIER]);
    filenames[NOZNICE] = findFilesInFolder(basepath+labels[NOZNICE]);
    filenames[POZADIE] = findFilesInFolder(basepath+labels[POZADIE]);

    vector<vector<vector<float>>> descriptors(4);

    // 3 lebo pozadie ignorujeme - svmka trenovane s pozadim ako negativnymi
    // prikladmi nefungovali, nic neklasifikovali.
    for (int i=0; i<3; i++){
        computeDescriptors(basepath,filenames[i],i,descriptors[i], hog);
    }

    for (int i=0; i<3; i++){
        trainSVM(i, descriptors);
    }

    return 0;
}