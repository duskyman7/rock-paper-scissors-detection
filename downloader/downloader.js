// Simple http GET request image downloader. For each line in given file tries
// to make a GET request and save the retrieved data as an image file.

if (process.argv.length < 3){
  console.log();
  console.log("Error: No file specified. Please provide file with links to images");
  console.log();
  console.log("usage: node " + process.argv[1] + " <file> [limit]");
  return;
}

var LIMIT = 10;

if (process.argv.length > 3){
  LIMIT = +process.argv[3];
}
console.log("using file: " + process.argv[2]);
console.log("Limit set to: " + LIMIT);
console.log();

const http = require('http');
const https = require('https');
const fs = require('fs');
const readline = require('readline');

var urlFileStream = fs.createReadStream(process.argv[2]);

const rl = readline.createInterface({
  input: urlFileStream
});

var index = 0;


rl.on("line", function (line){
  index++;

  if (index > LIMIT){
    rl.close();
    return;
  }

  // const imageName = line.split(/\s+/)[0];
  // const url = line.split(/\s+/)[1];
const url = line;
  // console.log(index+": " + url);

  if (String(url.split(".")[url.split(".").length-1]).toLowerCase() !== "jpg"){
    return;
  }

  ((i,name)=>{
    ((url.split(":")[0] === "https") ? https : http).get(url,(response)=>{
      const statusCode = response.statusCode;
      const contentType = response.headers['content-type'];
      // console.log("status: " + statusCode + ", " + contentType +", ("+url+")");

      if (statusCode !== 200){
        return;
      }

      if(!contentType || contentType.split("/")[0] !== 'image'){
        return;
      }

      var file = fs.createWriteStream("images/"+name+"."+contentType.split("/")[1], {
        defaultEncoding: 'binary'
      });

      response.pipe(file);

      file.on('close',()=>{
        console.log(i + ": SAVED: " + url);
      });

    }).on('error', (e)=>{
      console.log("ERROR: " + e.host);
    });
  })(index, index);
});



// http.get();
