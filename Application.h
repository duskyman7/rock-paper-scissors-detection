//
// Created by duso on 06-Apr-17.
//

#ifndef OPENCVTEST_APPLICATION_H
#define OPENCVTEST_APPLICATION_H

#include "FrameContainer.h"

// Core class of the application. Contains the main cycle of the program.
class Application
{
public:
    Application(int argc, char *argv[]);
    void run();

private:
    // methods
    void processInput(int key);
    void loadSVMs();
    void computeContours(Mat &inputMat, Mat &outputMat, vector<Vec4i> &hierarchy, vector<vector<Point>> &contours);
    void computeHull(vector<vector<Point>> &hull, vector<vector<int>> &inthull, vector<Point> &contours);
    void recordVideoBackground();
    int getLargestContourId(vector<vector<Point>> &contours);
    Mat getRgbDiff();
    Mat erodil(Mat &inputMat);
    Mat dilero(Mat &mat);
    Rect createBoundingBox(vector<Point> &hull);

    // members
    FrameContainer m_fc;
    VideoCapture m_vc;
    HOGDescriptor m_hog;
    Size m_winSize;

    // binary SVMs, one for each class
    Ptr<ml::SVM> m_svm_kamen;
    Ptr<ml::SVM> m_svm_papier;
    Ptr<ml::SVM> m_svm_noznice;

    Mat m_frame;

    // cached images of gestures, which will be recognized
    Mat m_image_stone;
    Mat m_image_paper;
    Mat m_image_sciss;
    Mat m_image_unkno; // if the gesture cannot be classified

    // threshold value, can be altered by + and -
    int m_thresh_val = 30;
    const int CANNY_LOW  = 100;
    const int CANNY_HIGH = 200;

    // pressed key
    int m_key;

    // Keys that application listens to.
    enum Keys{
        KEY_ESCAPE = 0x1b, // closes the program
        KEY_PLUS = 0x3d,   // increases threshold value
        KEY_MINUS = 0x2d,  // decreases threshold value
        KEY_SPACE = 0x20   // retakes the background image
    };

    int predictCategory(Mat &sample);
};


#endif //OPENCVTEST_APPLICATION_H
