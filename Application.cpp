//
// Created by duso on 06-Apr-17.
//

#include "Application.h"



// Takes action according to which key was pressed
void Application::processInput(int key)
{
    switch (key) {
        // decrease threshold
        case KEY_MINUS: {
            if (--m_thresh_val < 0) {
                m_thresh_val = 0;
            }
            cout << "threshold set to: " << m_thresh_val << endl;
            break;
        }
        // increase threshold
        case KEY_PLUS:
        {
            if (++m_thresh_val > 255) {
                m_thresh_val = 255;
            }
            cout << "threshold set to: " << m_thresh_val << endl;
            break;
        }
        // retake background
        case KEY_SPACE:
        {
            recordVideoBackground();
        }
    }
}

// Calculates the RGB difference between two given frames
Mat Application::getRgbDiff()
{
    Mat diff = abs(m_frame - m_fc.getAverageImage());

    Mat channels[3];

    split(diff, channels);

    Mat acc = Mat::zeros(channels[0].size(), CV_32F);

    for (auto &c : channels) {
        accumulate(c, acc);
    }
    acc.convertTo(acc, CV_8UC3);
    return acc;
}

// Combined erode and dilate function
Mat Application::erodil(Mat &input)
{
    Mat blurred1;
    Mat blurred2;
    erode(input, blurred1, Mat());
    dilate(blurred1, blurred2, Mat());
    return blurred2;
}

// Finds contours in the given frame. Output from Canny edge detector is pro-
// cessed by dilation and erosion to make the contours more continuous.
void Application::computeContours(Mat &inputMat, Mat &outputMat, vector<Vec4i>& hierarchy,
                                  vector<vector<Point>>& contours)
{
    Canny(inputMat, outputMat, CANNY_LOW, CANNY_HIGH);
    findContours(dilero(outputMat), contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
}

// Computes and saves the hull points and their indexes.
void Application::computeHull(vector<vector<Point>> &hull, vector<vector<int>> &inthull, vector<Point> &contours)
{
    convexHull(Mat(contours), hull[0]);
    convexHull(Mat(contours), inthull[0]);
}

// Main loop of the program.
void Application::run()
{
    recordVideoBackground();

    // Escape shuts down the program.
    while ((m_key = waitKey(1)) != KEY_ESCAPE) {

        processInput(m_key);

        m_vc.read(m_frame);

        Mat mat_diffed = getRgbDiff();
        Mat mat_thresholded;

        threshold(mat_diffed, mat_thresholded, m_thresh_val, 255, CV_THRESH_BINARY);

        Mat mat_erodilated = erodil(mat_thresholded);

        vector<Vec4i> hierarchy;
        vector<vector<Point>> contours;
        vector<vector<Point>> hull(1);
        vector<vector<int>> inthull(1);

        Mat mat_canny;

        computeContours(mat_erodilated, mat_canny, hierarchy, contours);

        int lci = getLargestContourId(contours);

        // skip the rest of the operations, if there are no contours
        if (lci == -1){
            continue;
        }

        computeHull(hull,inthull,contours[lci]);

        Mat drawing = Mat::zeros(m_frame.size(), CV_8UC3);

        // draw the contours of the hand
        drawContours(drawing,contours,lci,Scalar(0,255,0),1);

        // draw the hull
        drawContours(drawing,hull,0,Scalar(0,255,0),1);

        Rect boundRect = createBoundingBox(hull[0]);

        // draw the bounding box around hull
        rectangle( drawing, boundRect.tl(), boundRect.br(), Scalar(0,0,255), 1);

        Mat cropped = m_frame(boundRect);
        Mat resized;
        Mat gray;

        resize(cropped, resized, m_winSize);
        cvtColor(resized, gray, CV_BGR2GRAY);

        vector<float> descriptors;

        m_hog.compute(resized,descriptors);

        Mat sample(1,descriptors.size(),CV_32F);

        // suggestion from StackOverflow: http://answers.opencv.org/question/81831/convert-stdvectordouble-to-mat-and-show-the-image/
        mempcpy(sample.data,descriptors.data(),descriptors.size()* sizeof(float));

        int category = predictCategory(sample);

        String category_str = "Nothing";

        Mat *category_ptr = &m_image_unkno;

        switch(category){
            case 0: category_str = "Kamen"; category_ptr = &m_image_stone; break;
            case 1: category_str = "Papier"; category_ptr = &m_image_paper; break;
            case 2: category_str = "Noznice"; category_ptr = &m_image_sciss; break;
        }

        cout << category_str << endl;

        imshow("diff", mat_diffed);
//        imshow("threshold", mat_thresholded);
//        imshow("erodil", mat_erodilated);
//        imshow("canny", mat_canny);
//        imshow("drawing", drawing);
        imshow("resized", resized);
        imshow("predicted", *category_ptr);
    }
}

Application::Application(int argc, char *argv[])
{
    m_fc = FrameContainer(10);
    m_fc.setPreprocess(false);
    m_winSize = Size(96,128);

    // all sizes are default except windowsize
    m_hog = HOGDescriptor(m_winSize,Size(16,16), Size(8,8), Size(8,8), 9);

    m_vc = VideoCapture(0);
    m_vc.set(CV_CAP_PROP_EXPOSURE, 0);
    m_vc.set(CV_CAP_PROP_GAIN, 0);

    try{
        m_image_stone = imread("obrazky/kamen.jpg");
        m_image_paper = imread("obrazky/papier.jpeg");
        m_image_sciss = imread("obrazky/noznice.jpg");
        m_image_unkno = imread("obrazky/unknown.jpg");
    }catch (exception e){
        cerr << "Could not load gesture images!" << endl;
        exit(-1);
    }

    loadSVMs();
}

// Record and store the frames in FrameContainer until it is full
void Application::recordVideoBackground()
{
    m_fc.clear();

    while (!m_fc.ready()) {
        m_vc.read(m_frame);
        m_fc.addFrame(m_frame);
    }
}
// Finds countour covering the largest area. Returns position of the
// contour in the vector.
int Application::getLargestContourId(vector<vector<Point>> &contours)
{
    int largest_contour_id = -1;
    double area = 0;

    for (int i=0; i<contours.size();i++){
        if (largest_contour_id == -1){
            largest_contour_id = i;
            area = contourArea(contours[i]);
        }
        else if (contourArea(contours[i]) > area){
            largest_contour_id = i;
            area = contourArea(contours[i]);
        }
    }

    return largest_contour_id;
}

// Tries to load trained SVMs
void Application::loadSVMs()
{
    try{
        m_svm_kamen = ml::SVM::load("svm_kamen");
        m_svm_papier = ml::SVM::load("svm_papier");
        m_svm_noznice = ml::SVM::load("svm_noznice");
    }catch (exception){
        cerr << "Could not load SVM files!" << endl;
        exit(-2);
    }
    m_svm_kamen->setC(0.8);
    m_svm_papier->setC(0.8);
    m_svm_noznice->setC(0.8);
}

// Combined dilation and erosion function
Mat Application::dilero(Mat &mat)
{
    Mat midMat;
    Mat output;
    dilate(mat,midMat,Mat());
    erode(midMat, output,Mat());
    return output;
}

// creates bounding box around given hull with slight margins around
Rect Application::createBoundingBox(vector<Point> &hull)
{
    Rect boundRect = boundingRect( Mat(hull) );
    boundRect.height = min(boundRect.height, 350); // crop the contours if the hand is too long

    boundRect.y = max(boundRect.y - 10, 0);
    boundRect.height = min(boundRect.height+10, m_frame.size().height-boundRect.y);

    int xcenter = boundRect.x + boundRect.width/2;
    int new_width = boundRect.width/2 + 20;

    boundRect.x = max(xcenter - new_width,0);
    boundRect.width = min(new_width*2,m_frame.size().width - boundRect.x);

    return boundRect;
}

// Returns predicted category for a given sample
int Application::predictCategory(Mat &sample)
{
    if (m_svm_kamen->predict(sample) == 1){
        return 0;
    }
    else if (m_svm_papier->predict(sample) == 1){
        return 1;
    }
    else if (m_svm_noznice->predict(sample) == 1){
        return 2;
    }
    else return -1;
}
